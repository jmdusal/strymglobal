<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth; 










// Route::get('/', function () {
// 	return view('welcome');
// });
Route::get('/', 'FrontPageController@homepage')->name('homepage');

Auth::routes();

Route::get('/about', 'FrontPageController@about')->name('about');

Route::get('/applynow', 'FrontPageController@applynow')->name('applynow');
Route::post('applynow/submit', 'FrontPageController@submitapplynow')->name('mail.attachment');

Route::get('/contact', 'FrontPageController@contact')->name('contact');
Route::post('contact_us/submit', 'FrontPageController@submitcontactus')->name('sending.mail');

Route::get('/privacy_policy', 'FrontPageController@privacy_policy')->name('privacy_policy');
Route::get('/terms_conditions', 'FrontPageController@terms_conditions')->name('terms_conditions');