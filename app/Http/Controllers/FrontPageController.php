<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;

class FrontPageController extends Controller
{
	public function homepage(){
		return view('welcome-body');
	}

	public function about(){
		return view('web-routes.about');
	}

	public function applynow(){
		return view('web-routes.applynow');
	}

	public function privacy_policy(){
		return view('web-routes.privacy_policy');
	}

	public function terms_conditions(){
		return view('web-routes.terms_conditions');
	}

	
	public function submitapplynow(Request $request){
		$arr = array(
			'name_arr'			=>	$request->name,
			'to_email_arr'		=>	$request->to_email,
			'from_email_arr'	=>	$request->from_email,
			'address'			=>	$request->address,
			'applying_for'		=>	$request->applying_for,
		);
		$files = $request->file('files');
		Mail::send('mail_applynow', $arr, function($message) use ($request,$files){
			$message->to('info@strymglobal.com', 'Strym Global')->subject('Strym Global (New Applicant) ');
			$message->from($request->to_email, $request->name, $request->from_email, $request->address, $request->applying_for, $request->subject);
			foreach($files as $file){
				$message->attach($file->getRealPath(), array(
					'as'	=> $file->getClientOriginalName(),
					'mime'	=> $file->getMimeType()));
			}
		}
	);
		$request->all();

		$success = true;
		$message = "Your request has been submitted. We will contact you directly for the feedback.<br> Thank you and Good luck!";
		return response()->json([
			'success' => $success,
			'message' => '<br>We will contact you directly for the feedback <br><br> Thank you and Good luck! <br><br> <a href="#" onClick="window.location.reload();return false;" class="boxed-btn3">ok</a>',
		]);

	}


	public function contact(){
		return view('web-routes.contact');
	}

	public function submitcontactus(Request $request){
		$arr = array(
			'name_arr'      	=>  $request->name,
			'message_arr'		=>	$request->message,
			'to_email_arr'     	=>  $request->to_email,
			'from_email_arr'	=>	$request->from_email,
			'subject_arr'		=>	$request->subject,
		);

		Mail::send('mail_contactus', $arr, function($message) use ($request){
			$message->to('info@strymglobal.com', 'Strym Global')->subject('Strym Global (Inquiry From '.$request->name.')');
			$message->from($request->to_email, $request->name, $request->from_email, $request->subject);
		});
		
		$request->all();

		$success = true;  
		$message = '<br>Message has been Sent... Thank you for contacting us! <br><br> <a href="#" onClick="window.location.reload();return false;" class="boxed-btn3">ok</a>';
		return response()->json([
			'success' => $success,
			'message' => $message,
		]);
	}
}
