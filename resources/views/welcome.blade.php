<!doctype html>
<html class="no-js" lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>@yield('title')</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSS here -->
    <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@800&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('design/css/stack-interface.css')}}">
    <link rel="stylesheet" href="{{ asset('design/css/iconsmind.css')}}">
    <link rel="stylesheet" href="{{ asset('design/css/socicon.css')}}">
    <link rel="stylesheet" href="{{ asset('design/css/flickity.css')}}">
    <link rel="stylesheet" href="{{ asset('design/css/slider.css')}}">

    <link rel="stylesheet" href="{{ asset('design/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('design/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{ asset('design/css/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{ asset('design/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{ asset('design/css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{ asset('design/css/nice-select.css')}}">
    <link rel="stylesheet" href="{{ asset('design/css/flaticon.css')}}">
    <link rel="stylesheet" href="{{ asset('design/css/gijgo.css')}}">
    <link rel="stylesheet" href="{{ asset('design/css/animate.min.css')}}">
    <link rel="stylesheet" href="{{ asset('design/css/slick.css')}}">
    <link rel="stylesheet" href="{{ asset('design/css/slicknav.css')}}">
    <link rel="stylesheet" href="{{ asset('design/css/style.css')}}">

    <link rel="stylesheet" href="{{ asset('design/css/custom.css')}}">

    <!-- LOGO ICON -->
    <link rel="icon" href="{{ asset('design/img/strymglobal-icon.png')}}" type="image/gif" sizes="16x16">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">



    <link rel="stylesheet" type="text/css" href="{{asset('css/sweetalert2.min.css')}}">
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>

    <link href="{{ asset('filer/css/jquery.filer.css')}}" type="text/css" rel="stylesheet" />
    <link href="{{ asset('filer/css/themes/jquery.filer-dragdropbox-theme.css')}}" type="text/css" rel="stylesheet" />
    <link href="https://cdn.rawgit.com/sachinchoolur/lightgallery.js/master/dist/css/lightgallery.css" rel="stylesheet">

    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>


    <style>
        b {
            color: #435c75;
        }
    </style>


    @yield('stylesheet')
</head>

<body>

    <!-- Load Facebook SDK for JavaScript -->
    <div id="fb-root"></div>
    <script>
        window.fbAsyncInit = function() {
          FB.init({
            xfbml            : true,
            version          : 'v8.0'
        });
      };

      (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

    <!-- Your Chat Plugin code -->
    <div class="fb-customerchat"
    attribution=setup_tool
    page_id="111962637097023"
    theme_color="#435C75">
</div>

<!-- header-start -->
<header>
    <div class="header-area">
        <div id="sticky-header" class="main-header-area">
            <div class="container-fluid p-0">
                <div class="row align-items-center no-gutters">
                    <div class="col-xl-2 col-lg-2">
                        <div class="logo-img">
                            <a href="/">
                                <img src="{{ asset('design/img/strymglobal-logo.png')}}" style="width: 160px; margin-bottom: 0px" alt="">
                            </a>
                        </div>
                    </div>

                    <div class="col-xl-10 col-lg-10">
                        <div class="main-menu  d-none d-lg-block text-right">
                            <nav>
                                <ul id="navigation">
                                    <li><a class="active" href="{{url('/')}}">home</a></li>

                                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="{{url('/applynow')}}">Apply Now</a></li>

                                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="{{url('/about')}}">About Us</a></li>

                                    <li><a href="{{url('/contact')}}">Contact Us</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="mobile_menu d-block d-lg-none"></div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</header>
<!-- header-end -->



@yield('content')



<!-- footer start -->
<footer class="footer">
    <div class="footer_top p-4">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-2 col-md-3">
                    <div class="footer_logo wow fadeInRight" data-wow-duration="1s" data-wow-delay=".3s">
                        <a href="index.html">
                            <img src="{{ asset('footer-logo.png')}}" style="width: 160px;" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-xl-8 col-lg-8 col-md-9">
                    <div class="menu_links text-center">
                        <ul>
                                <li><a class="wow fadeInDown" data-wow-duration="1s" data-wow-delay=".2s" href="/applynow">Apply Now</a class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".3s"></li>
                                <li><a class="wow fadeInDown" data-wow-duration="1s" data-wow-delay=".4s" href="/about">About Us</a></li>
                                <li><a class="wow fadeInDown" data-wow-duration="1s" data-wow-delay=".6s" href="/contact">Contact Us</a></li>
                                <li><a class="wow fadeInDown" data-wow-duration="1s" data-wow-delay=".8s" href="/terms_conditions">Term of use</a></li>
                                <li><a class="wow fadeInDown" data-wow-duration="1s" data-wow-delay="1s" href="/privacy_policy">Privacy Policy</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-12" align="right">
                        <div class="socail_links">
                            <ul>
                                <li><a class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".3s" href="https://www.facebook.com/STRYMGlobal"> <i class="fab fa-facebook-f"></i> </a></li>
                                <li><a class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".4s" href="https://www.linkedin.com/company/strym-global-services/?viewAsMember=true"> <i class="fab fa-linkedin-in"></i> </a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copy-right_text">
            <div class="container">
                <div class="footer_border"></div>
                <div class="row">
                    <div class="col-xl-12">
                        <p class="copy_right text-center wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.3s">
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            <script>document.write(new Date().getFullYear());</script> STRYM Global. All rights reserved.
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--/ footer end  -->


    <!-- JS here -->
    <script src="{{ asset('design/js/flickity.min.js')}}"></script>
    <script src="{{ asset('design/js/scripts.js')}}"></script>

    <script src="{{ asset('design/js/vendor/modernizr-3.5.0.min.js')}}"></script>
    <script src="{{ asset('design/js/vendor/jquery-1.12.4.min.js')}}"></script>
    <script src="{{ asset('design/js/popper.min.js')}}"></script>
    <script src="{{ asset('design/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('design/js/owl.carousel.min.js')}}"></script>
    <script src="{{ asset('design/js/isotope.pkgd.min.js')}}"></script>
    <script src="{{ asset('design/js/ajax-form.js')}}"></script>
    <script src="{{ asset('design/js/waypoints.min.js')}}"></script>
    <script src="{{ asset('design/js/jquery.counterup.min.js')}}"></script>
    <script src="{{ asset('design/js/imagesloaded.pkgd.min.js')}}"></script>
    <script src="{{ asset('design/js/scrollIt.js')}}"></script>
    <script src="{{ asset('design/js/jquery.scrollUp.min.js')}}"></script>
    <script src="{{ asset('design/js/wow.min.js')}}"></script>
    <script src="{{ asset('design/js/nice-select.min.js')}}"></script>
    <script src="{{ asset('design/js/jquery.slicknav.min.js')}}"></script>
    <script src="{{ asset('design/js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{ asset('design/js/plugins.js')}}"></script>
    <script src="{{ asset('design/js/gijgo.min.js')}}"></script>

    <!--contact js-->
    <script src="{{ asset('design/js/contact.js')}}"></script>
    <script src="{{ asset('design/js/jquery.ajaxchimp.min.js')}}"></script>
    <script src="{{ asset('design/js/jquery.form.js')}}"></script>
    <script src="{{ asset('design/js/jquery.validate.min.js')}}"></script>
    <script src="{{ asset('design/js/mail-script.js')}}"></script>
    <script src="{{ asset('design/js/main.js')}}"></script>


    <!-- Sweet Alert js -->
    <script src="{{ asset('js/sweetalert2.all.min.js')}}"></script>
    <script src="{{ asset('js/jquery.form.min.js')}}"></script>

    <!-- jQuery Filer js -->
    <script src="{{ asset('filer/examples/dragdrop/js/custom.js')}}"></script>
    <script src="{{ asset('filer/js/jquery.filer.js')}}"></script>
    <script src="{{ asset('filer/js/jquery.filer.min.js')}}"></script>

    <!-- Light Gallery js -->
    <script src="https://cdn.rawgit.com/sachinchoolur/lightgallery.js/master/dist/js/lightgallery.js"></script>
    <script src="https://cdn.rawgit.com/sachinchoolur/lg-pager.js/master/dist/lg-pager.js"></script>
    <script src="https://cdn.rawgit.com/sachinchoolur/lg-autoplay.js/master/dist/lg-autoplay.js"></script>
    <script src="https://cdn.rawgit.com/sachinchoolur/lg-fullscreen.js/master/dist/lg-fullscreen.js"></script>
    <script src="https://cdn.rawgit.com/sachinchoolur/lg-zoom.js/master/dist/lg-zoom.js"></script>
    <script src="https://cdn.rawgit.com/sachinchoolur/lg-hash.js/master/dist/lg-hash.js"></script>
    <script src="https://cdn.rawgit.com/sachinchoolur/lg-share.js/master/dist/lg-share.js"></script>

</body>
</html>