@extends('welcome')

@section('title')
STRYM Global
@endsection

@section('content')

<div class="bradcam_area breadcam_bg_2">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="bradcam_text">
                    <h3>About Us</h3>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Mission and Vision start-->
<!-- <div class="container box_1170" style="padding-top: 60px; padding-bottom: 60px;  max-width: 1140px;">
    <div class="p-0">
        <div class="row">
            <div class="col-md-6">
                <div class="single-defination">
                    <h3 class="wow fadeInUp mb-20 text-center" data-wow-duration="1s" data-wow-delay=".1s">Our Mission</h3>
                    <p class="wow fadeInUp text-center" data-wow-duration="1s" data-wow-delay=".2s">Our Mission is to focus our resources in selecting the optimal partners with aligned attitudes and fostering their competencies through comprehensive training and personal development programs, guided by our 5 Pillars of personal development, to ultimately ensure that the service we provide optimized.</p>

                    <br>
                </div>
            </div>
            <div class="col-md-6">
                <div class="single-defination">
                    <h3 class="wow fadeInUp mb-20 text-center" data-wow-duration="1s" data-wow-delay=".3s">Our Vision</h3>
                    <p class="wow fadeInUp text-center" data-wow-duration="1s" data-wow-delay=".4s">Our Vision is to contribute to the development of rural areas in the Philippines by finding people with unrealized potential and nurturing them to be able to reach it.</p>

                    <p class="wow fadeInUp text-center" data-wow-duration="1s" data-wow-delay=".5s">We aim to aptly equip every employee with the skills and tools necessary to achieve success in their professional roles, and their personal lives, so that they may develop not just professionally, but holistically with integrity.</p>

                    <br>
                </div>
            </div>

        </div>
    </div>
</div> -->
<!--/ Mission and Vision end-->


<div class="mobile mb-15" hidden>

    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="testmonial_active owl-carousel">
                    <div class="single_carousel">
                        <div class="single_testmonial text-center">
                          <br>
                          <br>
                          <h3 class="wow fadeInUp mt-15 mb-20 text-center" data-wow-duration="1s" data-wow-delay=".1s">Our Mission</h3>
                          <p class="wow fadeInUp text-center" data-wow-duration="1s" data-wow-delay=".2s">Our Mission is to focus our resources in selecting the optimal partners with aligned attitudes and fostering their competencies through comprehensive training and personal development programs, guided by our 5 Pillars of personal development, to ultimately ensure that the service we provide optimized.</p>
                      </div>
                  </div>
                  <div class="single_carousel">
                    <div class="single_testmonial text-center">
                        <br>
                        <h3 class="wow fadeInUp mt-10 mb-20 text-center" data-wow-duration="1s" data-wow-delay=".3s">Our Vision</h3>
                        <p class="wow fadeInUp text-center" data-wow-duration="1s" data-wow-delay=".4s">Our Vision is to contribute to the development of rural areas in the Philippines by finding people with unrealized potential and nurturing them to be able to reach it.</p>

                        <p class="wow fadeInUp text-center" data-wow-duration="1s" data-wow-delay=".5s">We aim to aptly equip every employee with the skills and tools necessary to achieve success in their professional roles, and their personal lives, so that they may develop not just professionally, but holistically with integrity.</p>

                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


</div>




<section class="mt-30 mb-50 web"  >
    <div class="container" style="max-width: 75%; ">
        <div class="row">
            <div class="col">
                <div class="slider" data-arrows="true" data-paging="false">
                    <ul class="slides">
                        <li>
                            <img alt="Image" src="{{ asset('design/img/banner/mission.png')}}" />
                        </li>
                        <li>
                            <img alt="Image" src="{{ asset('design/img/banner/vision.png')}}" />
                        </li>
                    </ul>
                </div>
            </div>
            <!--end of col-->
        </div>
        <!--end row-->
    </div>
    <!--end of container-->
</section>



<section class="elements-title space--xxs text-center">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-10">
                <div class="section_title text-center mt-10 mb-0">
                    <h3 class="wow fadeInUp mb-0" data-wow-duration="1.2s" data-wow-delay=".2s" style="color:#334355; padding-bottom: 0">5 Pillars of STRYM Personal Development</h3>
                </div>
            </div>
        </div>
    </div>
    <!--end of container-->
</section>

<section class="mt-30 web">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="slider" data-arrows="false" data-paging="true" data-freescroll="false">
                    <ul class="slides service_area">
                        <li>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="single_service text-center wow fadeInLeft" data-wow-duration="1.2s" data-wow-delay=".4s" style="height: 290px;">
                                        <h3 class="mt-0">Self</h3>
                                        <p>Self is our responsibility to provide a culture and atmosphere conducive to growth and development. We believe that outward development starts with inward growth.</p>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="single_service text-center wow fadeInUp" data-wow-duration="1s" data-wow-delay=".3s" style="height: 290px;">
                                        <h3 class="mt-0">Technical</h3>
                                        <p>Technical is our responsibility to equip everyone with the skills needed for mastery. It's the science of achievement.</p>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="single_service text-center wow fadeInRight" data-wow-duration="1.2s" data-wow-delay=".4s" style="height: 290px;">
                                        <h3 class="mt-0">Young</h3>
                                        <p>Young is our responsibility to ensure that we address the physical need of our employees. Energy is generated from a physical body.</p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="row justify-content-center">
                                <div class="col-md-4">
                                    <div class="single_service text-center wow fadeInLeft" data-wow-duration="1.2s" data-wow-delay=".4s" style="height: 290px;">
                                        <h3 class="mt-0">Relationships</h3>
                                        <p>Relationships is our responsibility to create meaningful interactions with our family and friends, our co-workers and clients.</p>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="single_service text-center wow fadeInUp" data-wow-duration="1s" data-wow-delay=".3s" style="height: 290px;">
                                        <h3 class="mt-0">Money</h3>
                                        <p>Money is our responsibility to educate our employees to build wealth and financial IQ. We know that money is a tool that can be used for the development of societies.</p>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <!--end of col-->
        </div>
        <!--end row-->
    </div>
    <!--end of container-->
</section>



<section class="mobile" hidden>
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="slider">
                    <ul class="slides service_area">
                        <li>
                           <div class="single_service text-center wow fadeInLeft" data-wow-duration="1.2s" data-wow-delay=".4s" style="height: 290px;">
                            <h3 class="mt-0">Self</h3>
                            <p>
                                Self is our responsibility to provide a culture and atmosphere conducive to growth and development. We believe that outward development starts with inward growth.
                            </p>
                        </div>
                    </li>
                    <li>
                        <div class="single_service text-center wow fadeInUp" data-wow-duration="1s" data-wow-delay=".3s" style="height: 290px;">
                            <h3 class="mt-0">Technical</h3>
                            <p>Technical is our responsibility to equip everyone with the skills needed for mastery. It's the science of achievement.</p>
                        </div>

                    </li>
                    <li>
                        <div class="single_service text-center wow fadeInRight" data-wow-duration="1.2s" data-wow-delay=".4s" style="height: 290px;">
                            <h3 class="mt-0">Young</h3>
                            <p>Young is our responsibility to ensure that we address the physical need of our employees. Energy is generated from a physical body.</p>
                        </div>

                    </li>

                    <li>            
                        <div class="single_service text-center wow fadeInLeft" data-wow-duration="1.2s" data-wow-delay=".4s" style="height: 290px;">
                            <h3 class="mt-0">Relationships</h3>
                            <p>Relationships is our responsibility to create meaningful interactions with our family and friends, our co-workers and clients.</p>
                        </div>
                    </li>

                    <li>   <div class="single_service text-center wow fadeInUp" data-wow-duration="1s" data-wow-delay=".3s" style="height: 290px;">
                        <h3 class="mt-0">Money</h3>
                        <p>Money is our responsibility to educate our employees to build wealth and financial IQ. We know that money is a tool that can be used for the development of societies.</p>
                    </div>
                </li>

            </ul>
        </div>
    </div>
    <!--end of col-->
</div>
<!--end row-->
</div>
<!--end of container-->
</section>



<!-- Why STRYM  -->
<br>
<div class="section_title text-center mt-20 mb-90 web">
    <div class="row justify-content-center">
        <div class="col-md-7" >
            <h3 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".3s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.3s;">WHY STRYM GLOBAL SERVICES</h3>
            <p class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".4s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.4s;">

             We are a young, dynamic company who is set out to becoming the biggest ESL Center in the Philippines. We provide quality equipment and maintain an office atmosphere conducive for learning and relationships. We provide training seminars and personal development programs for our employees so that they are cultivated holistically. We provide shuttle services and have a fully functional pantry.

            </p>

            <p class="wow fadeInUp mt-20" data-wow-duration="1s" data-wow-delay=".4s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.4s;">
                If you are a hardworking and motivated person who has good work ethic and who is a team player, don’t hesitate and apply!
            </p>


             <br>
             <a href="/applynow" class="boxed-btn3 wow fadeInUp mt-10 text-extra-bold" data-wow-duration="1s" data-wow-delay=".7s">Apply Now!</a> 

     </div>
 </div>
</div>


<div class="section_title text-center mt-20 mb-90 mobile" hidden>
    <div class="row justify-content-center">
        <div class="col-md-7 ml-2 mr-2">
            <h3 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".3s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.3s;">WHY STRYM GLOBAL SERVICES</h3>
            <p class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".4s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.4s;">

             We are a young, dynamic company who is set out to becoming the biggest ESL Center in the Philippines. We provide quality equipment and maintain an office atmosphere conducive for learning and relationships. We provide training seminars and personal development programs for our employees so that they are cultivated holistically. We provide shuttle services and have a fully functional pantry.
         </p>


         <p class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".4s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.4s;">
             If you are a hardworking and motivated person who has good work ethic and who is a team player, don’t hesitate and apply!
         </p>

         <br>

         <a href="/applynow" class="boxed-btn3 wow fadeInUp mt-10 text-extra-bold" data-wow-duration="1s" data-wow-delay=".7s">Apply Now!</a> 
         
     </div>
 </div>
</div>

<!-- /Why STRYM  -->



<div class="testimonial_area ">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="testmonial_active owl-carousel">
                    <div class="single_carousel">
                        <div class="single_testmonial text-center">
                            <div class="quote">
                                <img src="{{ asset('design/img/testmonial/quote.svg')}}" alt="">
                            </div>
                            <p>We believe in our people. We believe that if we nurture their growth <br> and we give them a place to showcase their skills and talents that they will <br> be able to contribute to the company and the community.</p>
                            <div class="testmonial_author">
                                <div class="thumb">
                                    <img src="{{ asset('design/img/testmonial/thumb5.png')}}" alt="">
                                </div>
                                <h3>Bryan Young</h3>
                                <span>Founder</span>
                            </div>
                        </div>
                    </div>
                    <div class="single_carousel">
                        <div class="single_testmonial text-center">
                            <div class="quote">
                                <img src="{{ asset('design/img/testmonial/quote.svg')}}" alt="">
                            </div>
                            <p>We wanted to create a place that's different from the regular office scene in Iligan. <br> A place where we can assure the growth of each employee. <br> We want to make it fun.</p>
                            <div class="testmonial_author">
                                <div class="thumb">
                                    <img src="{{ asset('design/img/testmonial/thumb6.png')}}" alt="">
                                </div>
                                <h3>Elpidio Cruz III</h3>
                                <span>Co Founder</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 




<script>
  if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)){
      $('.web').attr('hidden', true);
      $('.mobile').attr('hidden', false);

  }else{

      $('.web').attr('hidden', false);
      $('.mobile').attr('hidden', true);
  }

</script>

@endsection