@extends('welcome')

@section('title')
STRYM Global
@endsection

@section('stylesheet')
<style>
	input{
		-webkit-box-shadow: 0px 5px 5px 0px #dde5ee;
	}

	.qual_icon {
		font-size: 25px
	}

	.swal-footer {
		text-align: center !important;
	}
	
	.jFiler-input {
		width: 100% !important;
		max-width: 400px !important;
		
	}
	
</style>
@endsection

@section('content')


<div class="bradcam_area breadcam_bg_3">
	<div class="container">
		<div class="row">
			<div class="col-xl-12">
				<div class="bradcam_text">
					<h3>Grow With Us</h3>
				</div>
			</div>
		</div>
	</div>
</div>

<div data-scroll-index="0" class="get_in_tauch_area" style="padding-top: 60px; padding-bottom: 60px; ">
	<div class="container">

		<div class="row">


			<div class="col-lg-4 web">
				<div class="blog_right_sidebar">
					<aside class="single_sidebar_widget post_category_widget">
						<h4>We are hiring!</h4>
						<p>Office Based Online ESL Tutor (full-time) in Iligan City</p>
						<hr>
						<br>
						<h4 class="mb-7" style="margin-bottom: 20px" >Qualifications</h4>
						<ul class="list cat-list">
							<li>
								<div class="row">
									<div class="col-md-2" align="center">
										<i class="qual_icon fas fa-user-tie"></i>
									</div>
									<div class="col-md-10 col-s-10 col-xs-10">
										Fluent English Speaker
									</div>
								</div>	
							</li>
							<li>
								<div class="row" >
									<div class="col-md-2 col-s-2 col-xs-2" align="center">
										<i class="qual_icon fas fa-user-friends"></i>
									</div>
									<div class="col-md-10  col-s-10 col-xs-10">
										Good Communication and Social Skills
									</div>
								</div>	
							</li>

							<li>
								<div class="row">
									<div class="col-md-2 col-md-2 col-s-2 col-xs-2" align="center">
										<i class="qual_icon fas fa-user-graduate"></i>
									</div>
									<div class="col-md-10  col-s-10 col-xs-10">
										College Degree or Technincal Vocation
									</div>
								</div>	
							</li>

							<li>
								<div class="row">
									<div class="col-md-2 col-md-2 col-s-2 col-xs-2" align="center">
										<i class="qual_icon fas fa-user-cog"></i>
									</div>
									<div class="col-md-10  col-s-10 col-xs-10">
										No Experience Necessary
									</div>
								</div>	
							</li>


							<li>
								<div class="row">
									<div class="col-md-2 col-md-2 col-s-2 col-xs-2" align="center">
										<i class="qual_icon fas fa-user-edit"></i>
									</div>
									<div class="col-md-10  col-s-10 col-xs-10">
										Willing to undergo training
									</div>
								</div>	
							</li>


							<li>
								<div class="row">
									<div class="col-md-2 col-md-2 col-s-2 col-xs-2" align="center">
										<i class="qual_icon fas fa-user-clock"></i>
									</div>
									<div class="col-md-10  col-s-10 col-xs-10">
										Flexible Time Schedule
									</div>
								</div>	
							</li>

							<li>
								<div class="row">
									<div class="col-md-2 col-md-2 col-s-2 col-xs-2" align="center">
										<i class="qual_icon fas fa-user-check"></i>
									</div>
									<div class="col-md-10  col-s-10 col-xs-10">
										At least 18 years old
									</div>
								</div>	
							</li>
						</ul>
					</aside>
				</div>
			</div>



			<div class="col-lg-4 mobile" hidden>
				<div class="blog_right_sidebar">
					<aside class="single_sidebar_widget post_category_widget">
						<h4>We are hiring!</h4>
						<p>Office Based Online ESL Tutor (full-time) in Iligan City</p>
						<hr>
						<br>
						<h4 class="mb-7" style="margin-bottom: 20px" >Qualifications</h4>
						<ul class="list cat-list">
							<li>
								<i class="fas fa-user-tie" style="font-size: 15px !important"></i> &nbsp; Fluent English Speaker
							</li>
							<li>
								<i class="fas fa-user-friends" style="font-size: 15px !important"></i> &nbsp;Good Communication and Social Skills
							</li>
							<li>
								<i class="fas fa-user-graduate" style="font-size: 15px !important"></i> &nbsp;College Degree or Technincal Vocation
							</li>
							<li>
								<i class="fas fa-user-cog" style="font-size: 15px !important"></i> &nbsp;No Experience Necessary
							</li>
							<li>
								<i class="fas fa-user-edit" style="font-size: 15px !important"></i> &nbsp;Willing to undergo training
							</li>
							<li>
								<i class="fas fa-user-clock" style="font-size: 15px !important"></i> &nbsp;Flexible Time Schedule
							</li>
							<li>
								<i class="fas fa-user-check" style="font-size: 15px !important"></i> &nbsp;At least 18 years old
							</li>
						</ul>
					</aside>
					
				</div>
				
			</div>

			<div class="col-lg-8 mb-5 mb-lg-0">
				<div class="blog_item" style="-webkit-box-shadow: 0px 5px 5px 0px #dde5ee;">
					<div class="blog_details" style="padding-top: 20px; padding-right: 10px">
						<div class="section_title text-center mb-40">
							<h3 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".3s">Apply Now</h3>
							<p class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".4s">There is no "I" in Team! Do you want to be part of Iligans most Elite team of ESL Tutors? <br> Join the Fun! Enjoy the dynamic team and work with competitive fixed salary with generous bonuses and incentives!</p>

							<span class="wow fadeInUp mb-0" data-wow-duration="1s" data-wow-delay=".4s">Please fill up the from below to apply</span>
						</div>


						<form method="POST" action="{{url('applynow/submit')}}" enctype="multipart/form-data" class="form_submit">
							{{csrf_field()}}

							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Full Name</label>
										<input type="text" name="name" id="name" placeholder="Enter Full Name " onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'First Name'" class="wow fadeInUp form-control single-input" data-wow-duration="1s" data-wow-delay=".5s"data-cf-modified-18486b197ff4595cff15f6dd-="" required="true">
									</div>
								</div>	
								<div class="col-md-6">
									<div class="form-group">
										<label>Email Address</label>
										<input type="email" name="from_email" id="from_email" placeholder="Enter your email address" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Email address'" class="wow single-input fadeInUp form-control" data-wow-duration="1s" data-wow-delay=".6s" data-cf-modified-18486b197ff4595cff15f6dd-="" required="true">
									</div>
								</div>	
							</div>


							<div class="row" style="padding-top: 20px ">
								<div class="col-md-12">
									<div class="form-group" >
										<label>Address</label>
										<input type="text" name="address" id="address" placeholder="Enter your current address " onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'First Name'" class="wow fadeInUp form-control single-input" data-wow-duration="1s" data-wow-delay=".5s"data-cf-modified-18486b197ff4595cff15f6dd-="" required="true">
									</div>			
								</div>
							</div>

							<div class="row" style="padding-top: 20px ">
								<div class="col-md-12">
									<div class="form-group" >
										<label>Applying for</label>
										<select class="wow single-input fadeInUp form-control" name="applying_for" id="applying_for" required>
											<option value="Office Based Online ESL Tutor (full-time)" selected>Office Based Online ESL Tutor (full-time)</option>
										</select>
									</div>			
								</div>
							</div>
							<hr>



							<div class="row" style="padding-top: 20px ">
								<div class="col-md-12">
									
									<label>Please upload your resume below ( Your resume should be in PDF format) </label>
									<input type="file" id="filer_input" name="files[]" style="padding: 0" multiple="multiple" class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".7s">
									
								</div>
							</div>

							<input name="to_email" class="form-control" value="strym@turistara.com" id="to_email" hidden>

							<div class="row justify-content-center">
								<div class="col-md-12">
									<div class="submit_btn wow fadeInUp mt-5" data-wow-duration="1s" data-wow-delay=".8s">
										<button class="boxed-btn3 btn-block text-extra-bold" type="submit">Submit</button>
									</div>

								</div>
							</div>

						</form>

					</div>
				</div>
			</div>

			
		</div>

	</div>
</div>


<!-- testimonial_area  -->
<div class="testimonial_area ">
	<div class="container">
		<div class="row">
			<div class="col-xl-12">
				<div class="testmonial_active owl-carousel">
					<div class="single_carousel">
						<div class="single_testmonial text-center">
							<div class="quote">
								<img src="{{ asset('design/img/testmonial/quote.svg')}}" alt="">
							</div>
							<p>Our highly trained management throughly cares for the welfare <br> and competency of all of its empolyees</p>
							<div class="testmonial_author">
								<div class="thumb">
									<img src="{{ asset('design/img/testmonial/thumb1.png')}}" alt="">
								</div>
								<h3>Star Navidad</h3>
								<span>Team Leader</span>
							</div>
						</div>
					</div>
					<div class="single_carousel">
						<div class="single_testmonial text-center">
							<div class="quote">
								<img src="{{ asset('design/img/testmonial/quote.svg')}}" alt="">
							</div>
							<p>Working as an online ESL Tutor is never an easy job but STRYM<br>Global Services Company has always been making it easier</p>
							<div class="testmonial_author">
								<div class="thumb">
									<img src="{{ asset('design/img/testmonial/thumb2.png')}}" alt="">
								</div>
								<h3>Melca Yba&#241;ez</h3>
								<span>Team Leader</span>
							</div>
						</div>
					</div>
					<div class="single_carousel">
						<div class="single_testmonial text-center">
							<div class="quote">
								<img src="{{ asset('design/img/testmonial/quote.svg')}}" alt="">
							</div>
							<p>With the help of my colleagues, trainers and coaches who have undying support, <br> I won't be here without you guys! Arigatou gozaimasu and Kudos to us!</p>
							<div class="testmonial_author">
								<div class="thumb">
									<img src="{{ asset('design/img/testmonial/thumb3.png')}}" alt="">
								</div>
								<h3>Ena Delos Angeles</h3>
								<span>Batch 1</span>
							</div>
						</div>
					</div>
					<div class="single_carousel">
						<div class="single_testmonial text-center">
							<div class="quote">
								<img src="{{ asset('design/img/testmonial/quote.svg')}}" alt="">
							</div>
							<p>Self confidence and trust were some of the keys. Let your experiences as your strength.<br> STRYM Global made me realize all of those</p>
							<div class="testmonial_author">
								<div class="thumb">
									<img src="{{ asset('design/img/testmonial/thumb4.png')}}" alt="">
								</div>
								<h3>Lor Cadorniga</h3>
								<span>Batch 1</span>
							</div>
						</div>
					</div>
					<div class="single_carousel">
                        <div class="single_testmonial text-center">
                            <div class="quote">
                                <img src="{{ asset('design/img/testmonial/quote.svg')}}" alt="">
                            </div>
                            <p>I'm really greatful for the opportunity to work here. The people here also <br> treat you like family and the working environment doesn't have any toxicity</p>
                            <div class="testmonial_author">
                                <div class="thumb">
                                    <img src="{{ asset('design/img/testmonial/thumb7.png')}}" alt="">
                                </div>
                                <h3>Karla Ligaray</h3>
                                <span>Trainer</span>
                            </div>
                        </div>
                    </div>
                    <div class="single_carousel">
                        <div class="single_testmonial text-center">
                            <div class="quote">
                                <img src="{{ asset('design/img/testmonial/quote.svg')}}" alt="">
                            </div>
                            <p>Self confidence and trust were some of the keys. Let your experience as your strength.<br> STRYM Global made me realize all of those</p>
                            <div class="testmonial_author">
                                <div class="thumb">
                                    <img src="{{ asset('design/img/testmonial/thumb8.png')}}" alt="">
                                </div>
                                <h3>Kai Moralita</h3>
                                <span>Trainer</span>
                            </div>
                        </div>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /testimonial_area  -->


<script type="text/javascript">


	//swal("Your request has <br> been submitted!", 'We will contact you directly for the feedback <br><br> Thank you and Good luck!', "success");




	if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)){
		$('.web').attr('hidden', true);
		$('.mobile').attr('hidden', false);

	}else{

		$('.web').attr('hidden', false);
		$('.mobile').attr('hidden', true);
	}


	$(document).ready(function() {
		$('#filer_input').filer({
			limit: 1,
			maxSize: 3, //mb
			extensions: ["pdf"],
			itemAppendToEnd: true,
			removeConfirmation: true,
			showThumbs: true,
			allowDuplicates: false,
			addMore: false,
			captions: {
				button: "Choose Files",
				feedback: "Choose files To Upload",
				feedback2: "files were chosen",
				drop: "Drop file here to Upload",
				removeConfirmation: "Are you sure you want to remove this file?",
				errors: {
					filesType: "Only PDF file are allowed to be uploaded.",
					folderUpload: "You are not allowed to upload folders."
				}
			}
		});	

	});




	$(".form_submit").submit(function(e) {
		e.preventDefault();


		if ($('#filer_input').get(0).files.length === 0) {
			swal({
				title: 'No resume uploaded',
				html: 'Please upload your resume to submit.',
				type: 'info'
			});


			return false;
		}


		swal({
			html:   '<div class="loader-block">'+
			'<img src="'+'{{ asset("design/img/loading.gif")}}'+'" width="150" height="150">'+
			'</div>'+
			'<h4 class="text-center">Please Wait... <span id="progress-status"></span></h4>'+
			'<small><span id="TakeWhile" hidden> </span></small>', 
			allowOutsideClick: false,
			showConfirmButton:false,
		});
		setTimeout(function(){ $('#TakeWhile').prop('hidden', false) }, 2500);
		setTimeout(function(){

			$('.form_submit').ajaxSubmit({
				beforeSubmit: function(){
					$('.progress-bar').width('0%')
				},
				uploadProgress: function(event, position, total, percentComplete){
					$('#progress-status').text(percentComplete+'%');
				},            
				error: function (xhr, status, errorThrown) {
          //Here the status code can be retrieved like;
          xhr.status;
          swal({
          	title: "Error!",
          	text: "Please check your information and try again!",
          	type: "error",
          	showConfirmButton: true,
          });
          // location.reload(true);
          console.log(xhr.responseText);
      },
      success: function(results){
      	console.log(results);
      	if(results.success == true){
      		$('.progress-bar').width('0%').html('');

      		swal({
      			title: 'Succesfully Submitted!',
      			html:  results.message,
      			allowOutsideClick: false,
      			showConfirmButton:false,
      			type: 'success',
      		});
      		

      	}else{
      		swal({
      			title: "Error!",
      			text: results.message,
      			type: "error"
      		});
      		setTimeout(function(){
      			location.reload(true);
      		}, 1000);
      	}
      },
      resetForm: true
  });
		}, 1500);
	});


</script>


@endsection