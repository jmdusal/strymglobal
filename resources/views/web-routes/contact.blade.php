@extends('welcome')

@section('title')
STRYM Global
@endsection

@section('content')

<div class="bradcam_area breadcam_bg_4">
  <div class="container">
    <div class="row">
      <div class="col-xl-12">
        <div class="bradcam_text">
          <h3>Find Us</h3>
        </div>
      </div>
    </div>
  </div>
</div>


<section style="padding-top: 45px;" class="contact-section section_padding">
  <div class="container">
    <div class="row">


      <div class="col-lg-7">
        <h2 class="contact-title wow fadeInUp" data-wow-duration="1s" data-wow-delay=".3s"><b style="color: #435c75;">Contact</b> Us</h2>
        <p class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".4s">Feel free to ask for details, don`t save any questions!</p>



        <form method="POST" action="{{url('contact_us/submit')}}" novalidate="novalidate" enctype="multipart/form-data" class="form_submit">
          {{csrf_field()}}

          <div class="row">

            <div class="col-12">
              <div class="form-group">
                <textarea class="form-control w-100 wow fadeInUp" data-wow-duration="1s" data-wow-delay=".5s" name="message" id="message" cols="30" rows="9" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Enter Message'" placeholder="Enter Message" data-cf-modified-8e44969d1cad995a417b9b11-=""></textarea>
              </div>
            </div>


            <div class="col-sm-6">
              <div class="form-group">
                <input class="form-control wow fadeInUp" data-wow-duration="1s" data-wow-delay=".6s" name="name" id="name" type="text" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Enter your name'" placeholder="Enter your name" data-cf-modified-8e44969d1cad995a417b9b11-="">
              </div>
            </div>


            <div class="col-sm-6">
              <div class="form-group">
                <input class="form-control wow fadeInUp" data-wow-duration="1s" data-wow-delay=".7s" name="from_email" id="from_email" type="email" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Enter email address'" placeholder="Enter email address" data-cf-modified-8e44969d1cad995a417b9b11-="">
              </div>
            </div>



            <div class="col-12">
              <div class="form-group">
                <input class="form-control wow fadeInUp" data-wow-duration="1s" data-wow-delay=".8s" name="subject" id="subject" type="text" onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Enter Subject'" placeholder="Enter Subject" data-cf-modified-8e44969d1cad995a417b9b11-="">
              </div>
            </div>



            <input name="to_email" class="form-control" value="strym@turistara.com" id="to_email" hidden>


          </div>
          <div class="form-group mt-3">
            <button type="submit" class="button button-contactForm btn_4 boxed-btn3 wow fadeInUp text-extra-bold" data-wow-duration="1s" data-wow-delay=".8s">Send Message</button>
          </div>
        </form>
      </div>



      <div class="col-lg-5">

        <h3 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".3s">Our <b style="color: #435c75;">Office</b></h3>
        <br>

        <div class="media contact-info wow fadeInUp" data-wow-duration="1s" data-wow-delay=".4s">
          <span class="contact-info__icon"><i class="ti-home"></i></span>
          <div class="media-body">
            <h3>Brgy. Pala-o 9200 Iligan City</h3>
            <p>Philippines</p>
          </div>
        </div>

        <div class="media contact-info wow fadeInUp" data-wow-duration="1s" data-wow-delay=".5s">
          <span class="contact-info__icon"><i class="ti-tablet"></i></span>
          <div class="media-body">
            <h3>0926 172 9585</h3>
            <p>Always open</p>
          </div>
        </div>

        <div class="media contact-info wow fadeInUp" data-wow-duration="1s" data-wow-delay=".6s">
          <span class="contact-info__icon"><i class="ti-email"></i></span>
          <div class="media-body">
            <h3>info@strymglobal.com</h3>
            <p>Send us your query anytime!</p>
          </div>
        </div>

        <div class="wow fadeInUp" id="gMap" data-wow-duration="1s" data-wow-delay=".7s" style="width:100%;height:200px;"></div>

      </div>
    </div>
  </div>
</section>




<!-- testimonial_area  -->
<div class="testimonial_area ">
  <div class="container">
    <div class="row">
      <div class="col-xl-12">
        <div class="testmonial_active owl-carousel">
          <div class="single_carousel">
            <div class="single_testmonial text-center">
              <div class="quote">
                <img src="{{ asset('design/img/testmonial/quote.svg')}}" alt="">
              </div>
              <p>Our highly trained management throughly cares for the welfare <br> and competency of all of its empolyees</p>
              <div class="testmonial_author">
                <div class="thumb">
                  <img src="{{ asset('design/img/testmonial/thumb1.png')}}" alt="">
                </div>
                <h3>Star Navidad</h3>
                <span>Team Leader</span>
              </div>
            </div>
          </div>
          <div class="single_carousel">
            <div class="single_testmonial text-center">
              <div class="quote">
                <img src="{{ asset('design/img/testmonial/quote.svg')}}" alt="">
              </div>
              <p>Working as an online ESL Tutor is never an easy job but STRYM<br>Global Services Company has always been making it easier</p>
              <div class="testmonial_author">
                <div class="thumb">
                  <img src="{{ asset('design/img/testmonial/thumb2.png')}}" alt="">
                </div>
                <h3>Melca Yba&#241;ez</h3>
                <span>Team Leader</span>
              </div>
            </div>
          </div>
          <div class="single_carousel">
            <div class="single_testmonial text-center">
              <div class="quote">
                <img src="{{ asset('design/img/testmonial/quote.svg')}}" alt="">
              </div>
              <p>With the help of my colleagues, trainers and coaches who have undying support, <br> I won't be here without you guys! Arigatou gozaimasu and Kudos to us!</p>
              <div class="testmonial_author">
                <div class="thumb">
                  <img src="{{ asset('design/img/testmonial/thumb3.png')}}" alt="">
                </div>
                <h3>Ena Delos Angeles</h3>
                <span>Batch 1</span>
              </div>
            </div>
          </div>
          <div class="single_carousel">
            <div class="single_testmonial text-center">
              <div class="quote">
                <img src="{{ asset('design/img/testmonial/quote.svg')}}" alt="">
              </div>
              <p>Self confidence and trust were some of the keys. Let your experiences as your strength.<br> STRYM Global made me realize all of those</p>
              <div class="testmonial_author">
                <div class="thumb">
                  <img src="{{ asset('design/img/testmonial/thumb4.png')}}" alt="">
                </div>
                <h3>Lor Cadorniga</h3>
                <span>Batch 1</span>
              </div>
            </div>
          </div>
          <div class="single_carousel">
            <div class="single_testmonial text-center">
                <div class="quote">
                    <img src="{{ asset('design/img/testmonial/quote.svg')}}" alt="">
                </div>
                <p>I'm really greatful for the opportunity to work here. The people here also <br> treat you like family and the working environment doesn't have any toxicity</p>
                <div class="testmonial_author">
                    <div class="thumb">
                        <img src="{{ asset('design/img/testmonial/thumb7.png')}}" alt="">
                    </div>
                    <h3>Karla Ligaray</h3>
                    <span>Trainer</span>
                </div>
            </div>
          </div>
          <div class="single_carousel">
            <div class="single_testmonial text-center">
                <div class="quote">
                    <img src="{{ asset('design/img/testmonial/quote.svg')}}" alt="">
                </div>
                <p>Self confidence and trust were some of the keys. Let your experience as your strength.<br> STRYM Global made me realize all of those</p>
                <div class="testmonial_author">
                    <div class="thumb">
                        <img src="{{ asset('design/img/testmonial/thumb8.png')}}" alt="">
                    </div>
                    <h3>Kai Moralita</h3>
                    <span>Trainer</span>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /testimonial_area  -->




<script>
  function myMap() {
    const mapProp= {
      center:new google.maps.LatLng( 8.2275785,124.2459673 ),
      zoom:17,
    };
    const map = new google.maps.Map(document.getElementById("gMap"),mapProp);

  //   var icon = {
  //   url: "{{ asset('design/img/strymglobal-1.png')}}", // url
  //   scaledSize: new google.maps.Size(28, 38), // scaled size
  //   origin: new google.maps.Point(0,0), // origin
  //   anchor: new google.maps.Point(0, 0) // anchor
  // };

  const marker = new google.maps.Marker({
    position: { lat: 8.2275785, lng: 124.2459673 },
    animation:google.maps.Animation.BOUNCE,
    // icon:"https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png",
    icon:"{{ asset('design/img/strymglobal.png')}}",
    title: "Hello World!"
  });







  marker.setMap(map);
}



$(".form_submit").submit(function(e) {
  e.preventDefault();
  swal({
    html:   '<div class="loader-block">'+
    '<img src="'+'{{ asset("design/img/loading.gif")}}'+'" width="150" height="150">'+
    '</div>'+
    '<h4 class="text-center">Please Wait... <span id="progress-status"></span></h4>'+
    '<small><span id="TakeWhile" hidden> </span></small>', 
    allowOutsideClick: false,
    showConfirmButton:false,
  });
  setTimeout(function(){ $('#TakeWhile').prop('hidden', false) }, 2500);
  setTimeout(function(){
    $('.form_submit').ajaxSubmit({
      beforeSubmit: function(){
        $('.progress-bar').width('0%')
      },
      uploadProgress: function(event, position, total, percentComplete){
        $('#progress-status').text(percentComplete+'%');
      },            
      error: function (xhr, status, errorThrown) {
          //Here the status code can be retrieved like;
          xhr.status;
          swal({
            title: "Error!",
            text: "Please check your information and try again!",
            type: "error",
            showConfirmButton: true,
          });
          // location.reload(true);
          console.log(xhr.responseText);
        },
        success: function(results){
          console.log(results);
          if(results.success == true){
            
            $('.progress-bar').width('0%').html('');
            swal({
              title: "Done!",
              html: results.message,
              type: "success",
              showConfirmButton: false
            });

          }else{
            swal({
              title: "Error!",
              text: results.message,
              type: "error"
            });
            setTimeout(function(){
              location.reload(true);
            }, 1000);
          }
        },
        resetForm: true
      });
  }, 1500);
});


</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDii5Ge6RRt0XB710mymDHA-uK3TfgE3wQ&callback=myMap"></script>
@endsection

