@extends('welcome')

@section('title')
STRYM Global
@endsection

@section('content')
<!-- slider_area_start -->
<div class="slider_area" id="home">
    <div class="single_slider  d-flex align-items-center slider_bg_1" style="height: 500px">
        <div class="container">
            <div class="row align-items-center justify-content-start">
                <div class="col-lg-8 col-md-8">
                    <div class="slider_text">
                        <h3 class="wow fadeInLeft" data-wow-duration="1s" data-wow-delay=".1s">
                            The Culture that Cares, <br> Be A Strymer today
                        </h3>
                        <a href="/applynow" class="boxed-btn3 wow fadeInUp mt-10 text-extra-bold" data-wow-duration="1s" data-wow-delay=".2s">Apply Now!</a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="slider_text">
                        <img class="logo-responsive" src="{{ asset('design/img/logo-circle.png')}}" alt="Strym Logo">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- slider_area_end -->

<!-- service_area  -->
<div class="service_area">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-10">
                <div class="section_title text-center mt-30 mb-30">
                    <h3 class="wow fadeInUp" style="font-size: 35px !important" data-wow-duration="1.2s" data-wow-delay=".2s" >The Fastest Growing ESL Center in Iligan City</h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-4 col-md-4">
                <div class="single_service text-center wow fadeInLeft" style="height: 325px" data-wow-duration="1.2s" data-wow-delay=".4s">
                    <div class="icon">
                        <i class="fa fa-globe-asia" style="font-size: 50px; color: #334355"></i>
                    </div>
                    <h3>The Culture that Cares</h3>
                    <p>We are a great company because of the company we are with. We treat our Employee as our Partners. </p><br><br>
                </div>
            </div>      
            <div class="col-xl-4 col-md-4">
                <div class="single_service text-center wow fadeInRight" style="height: 325px" data-wow-duration="1.2s" data-wow-delay=".4s">
                    <div class="icon">
                        <i class="fa fa-handshake" style="font-size: 50px; color: #334355"></i>
                    </div>
                    <h3>We make You our Business</h3>
                    <p>Quality assurance is maintained to ensure the Quality Service for our clients.</p>
                </div>
            </div>
            <div class="col-xl-4 col-md-4">
                <div class="single_service text-center wow fadeInUp" style="height: 325px" data-wow-duration="1s" data-wow-delay=".3s">
                    <div class="icon">
                        <i class="fa fa-hands" style="font-size: 50px; color: #334355"></i>
                    </div>
                    <h3>Grow With Us</h3>
                    <p>We make it a point to maintain a high level of employee development to ensure the continual growth of our Partners. </p>
                    <br>
                </div>
            </div>
            
        </div>
    </div>
</div>
<!--/ service_area  end-->



<div class="about_area">
    <div class="container">
        <div class="row justify-content-end">
            <div class="col-lg-5 offset-lg-1">
                <div class="about_info">
                    <div class="section_title white_text">
                       
                        <h2 class="wow fadeInUp mb-4 text-white" data-wow-duration="1s" data-wow-delay=".3s"> Be part of the team! <br> Be a Strymer! </h2>
                        <p class="mid_text wow fadeInUp" data-wow-duration="1s" data-wow-delay=".5s">
                        "Providing quality services globally through technology by empowering Filipinos through constant training and education to contribute to the Global Community."</p>
                        <br>
                        <a href="/applynow" class="boxed-btn3 wow fadeInUp mt-10 text-extra-bold" data-wow-duration="1s" data-wow-delay=".7s">Apply Now!</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="apply_now_area">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="work_info">
                    <div class="section_title">
                        <h2 class="wow fadeInLeft mb-4" data-wow-duration="1s" data-wow-delay=".3s">We are in the process of Massive Expansion!</h2>
                        <p class="mid_text wow fadeInRight" data-wow-duration="1s" data-wow-delay=".5s">
                         "Our goal is to be the biggest ESL Center in the Philippines"
                     </p>   
                 </div>
                 <a href="/about" class="boxed-btn3 wow fadeInUp mt-10 text-extra-bold" data-wow-duration="1s" data-wow-delay=".7s">About Us</a>
             </div>
         </div>
     </div>
 </div>
</div>


<div class="gallery_area" style="padding-top: 60px; padding-bottom: 60px">
    <div class="container">
        <h2 class="text-center wow fadeInUp" data-wow-duration="1s" data-wow-delay=".3s">Our Gallery</h2>
        <div class="row gallery-item">
            <div class="col-md-4">
                <a href="{{ asset('design/img/gallery/strym-gallery-1.jpg')}}" class="img-pop-up">
                    <div class="single-gallery-image wow fadeInUp" data-wow-duration="1s" data-wow-delay=".4s" style="background: url({{ asset('design/img/gallery/strym-gallery-1.png')}}); height: 280px"></div>
                </a>
            </div>
            <div class="col-md-4">
                <a href="{{ asset('design/img/gallery/strym-gallery-2.png')}}" class="img-pop-up">
                    <div class="single-gallery-image wow fadeInUp" data-wow-duration="1s" data-wow-delay=".5s" style="background: url({{ asset('design/img/gallery/strym-gallery-2.png')}}); height: 280px"></div>
                </a>
            </div>
            <div class="col-md-4">
                <a href="{{ asset('design/img/gallery/strym-gallery-3.png')}}" class="img-pop-up">
                    <div class="single-gallery-image wow fadeInUp" data-wow-duration="1s" data-wow-delay=".6s" style="background: url({{ asset('design/img/gallery/strym-gallery-3.png')}}); height: 280px"></div>
                </a>
            </div>
            <div class="col-md-4">
                <a href="{{ asset('design/img/gallery/strym-gallery-4.png')}}" class="img-pop-up">
                    <div class="single-gallery-image wow fadeInUp" data-wow-duration="1s" data-wow-delay=".7s" style="background: url({{ asset('design/img/gallery/strym-gallery-4.png')}}); height: 280px"></div>
                </a>
            </div>
            <div class="col-md-4">
                <a href="{{ asset('design/img/gallery/strym-gallery-5.png')}}" class="img-pop-up">
                    <div class="single-gallery-image wow fadeInUp" data-wow-duration="1s" data-wow-delay=".8s" style="background: url({{ asset('design/img/gallery/strym-gallery-5.png')}}); height: 280px"></div>
                </a>
            </div>
            <div class="col-md-4">
                <a href="{{ asset('design/img/gallery/strym-gallery-6.png')}}" class="img-pop-up">
                    <div class="single-gallery-image wow fadeInUp" data-wow-duration="1s" data-wow-delay=".9s" style="background: url({{ asset('design/img/gallery/strym-gallery-6.png')}}); height: 280px"></div>
                </a>
            </div>
        </div>
    </div>
</div>


<!-- testimonial_area  -->
<div class="testimonial_area ">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="testmonial_active owl-carousel">
                    <div class="single_carousel">
                        <div class="single_testmonial text-center">
                            <div class="quote">
                                <img src="{{ asset('design/img/testmonial/quote.svg')}}" alt="">
                            </div>
                            <p>Our highly trained management throughly cares for the welfare <br> and competency of all of its empolyees</p>
                            <div class="testmonial_author">
                                <div class="thumb">
                                    <img src="{{ asset('design/img/testmonial/thumb1.png')}}" alt="">
                                </div>
                                <h3>Star Navidad</h3>
                                <span>Team Leader</span>
                            </div>
                        </div>
                    </div>
                    <div class="single_carousel">
                        <div class="single_testmonial text-center">
                            <div class="quote">
                                <img src="{{ asset('design/img/testmonial/quote.svg')}}" alt="">
                            </div>
                            <p>Working as an online ESL Tutor is never an easy job but STRYM<br>Global Services Company has always been making it easier</p>
                            <div class="testmonial_author">
                                <div class="thumb">
                                    <img src="{{ asset('design/img/testmonial/thumb2.png')}}" alt="">
                                </div>
                                <h3>Melca Yba&#241;ez</h3>
                                <span>Team Leader</span>
                            </div>
                        </div>
                    </div>
                    <div class="single_carousel">
                        <div class="single_testmonial text-center">
                            <div class="quote">
                                <img src="{{ asset('design/img/testmonial/quote.svg')}}" alt="">
                            </div>
                            <p>With the help of my colleagues, trainers and coaches who have undying support, <br> I won't be here without you guys! Arigatou gozaimasu and Kudos to us!</p>
                            <div class="testmonial_author">
                                <div class="thumb">
                                    <img src="{{ asset('design/img/testmonial/thumb3.png')}}" alt="">
                                </div>
                                <h3>Ena Delos Angeles</h3>
                                <span>Batch 1</span>
                            </div>
                        </div>
                    </div>
                    <div class="single_carousel">
                        <div class="single_testmonial text-center">
                            <div class="quote">
                                <img src="{{ asset('design/img/testmonial/quote.svg')}}" alt="">
                            </div>
                            <p>Self confidence and trust were some of the keys. Let your experiences as your strength.<br> STRYM Global made me realize all of those</p>
                            <div class="testmonial_author">
                                <div class="thumb">
                                    <img src="{{ asset('design/img/testmonial/thumb4.png')}}" alt="">
                                </div>
                                <h3>Lor Cadorniga</h3>
                                <span>Batch 1</span>
                            </div>
                        </div>
                    </div>
                    <div class="single_carousel">
                        <div class="single_testmonial text-center">
                            <div class="quote">
                                <img src="{{ asset('design/img/testmonial/quote.svg')}}" alt="">
                            </div>
                            <p>I'm really greatful for the opportunity to work here. The people here also <br> treat you like family and the working environment doesn't have any toxicity</p>
                            <div class="testmonial_author">
                                <div class="thumb">
                                    <img src="{{ asset('design/img/testmonial/thumb7.png')}}" alt="">
                                </div>
                                <h3>Karla Ligaray</h3>
                                <span>Trainer</span>
                            </div>
                        </div>
                    </div>
                    <div class="single_carousel">
                        <div class="single_testmonial text-center">
                            <div class="quote">
                                <img src="{{ asset('design/img/testmonial/quote.svg')}}" alt="">
                            </div>
                            <p>Self confidence and trust were some of the keys. Let your experience as your strength.<br> STRYM Global made me realize all of those</p>
                            <div class="testmonial_author">
                                <div class="thumb">
                                    <img src="{{ asset('design/img/testmonial/thumb8.png')}}" alt="">
                                </div>
                                <h3>Kai Moralita</h3>
                                <span>Trainer</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /testimonial_area  -->


@endsection